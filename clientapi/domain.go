package main

import (
	"clientapi/destination"
	"clientapi/source"
	"encoding/json"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"os"
	"time"
)

func main() {
	// env variables

	// turning on mocks allows test as application "lives" in isolation
	logLevel := "Info"
	areMocksEnabled := false
	if os.Getenv("APP_CLIENTAPI_MOCK_ENABLE") == "true" {
		areMocksEnabled = true
	}

	if os.Getenv("APP_CLIENTAPI_DEBUG") == "true" {
		logLevel = "DEBUG"
	}

	// Default level for this example is info, unless debug flag is present
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if logLevel == "DEBUG" {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	mainLoop(areMocksEnabled)
}

func mainLoop(mocksEnabled bool) {
	destinationServices := destination.GetDestinationServices()
	sourceServices := source.GetSourceServices()

	log.Info().Strs("ENV VARIABLES", os.Environ()).Send()
	log.Info().Msg("Starting application")

	var jsonDecoders []*json.Decoder
	dataAlreadyFetched := make(map[int]bool)

	for {
		log.Info().Msg("Source loop <-")

		// Inititialize data sources
		for id, sourceService := range sourceServices {
			if dataAlreadyFetched[id] {
				continue
			}
			jsonStream, err := sourceService.ReadPortsData()
			if err != nil {
				// fatal
			}
			jsonDecoders = append(jsonDecoders, jsonStream)
			dataAlreadyFetched[id] = true
		}

		// Initialize destinations
		log.Info().Msg("Destination loop ->")
		for _, destSvc := range destinationServices {
			err := destSvc.Init(mocksEnabled)
			if err != nil {
				// fatal
			}
			for _, dataSourceDecoder := range jsonDecoders {
				err := destSvc.SendData(dataSourceDecoder)
				if err != nil {
					// fatal
				}
			}
		}





		time.Sleep(1*time.Second)
	}
	log.Info().Msg("Application stopped.")

}
