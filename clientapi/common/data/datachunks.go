package data

import (
	"bytes"
	"encoding/json"

	"github.com/rs/zerolog/log"
)

func ProcessPortDataChunkFromBuffer(buf *bytes.Buffer) (PortsData, error) {
	funcName := "ProcessPortDataChunkFromBuffer"

	var portsData PortsData
	err := json.Unmarshal(buf.Bytes(), &portsData)
	if err != nil {
		log.Err(err).
			Str("Function", funcName).
			Interface("portsData", portsData).
			Msg("can't marshal this data")
		return portsData, err
	}

	// Clear buffer just in case
	buf.Reset()

	return portsData, nil
}
