package data

type PortsData map[string]PortsDataDetails

type PortsDataDetails struct {
	Name        string    `json:"name,omitempty"`
	City        string    `json:"city,omitempty"`
	Country     string    `json:"country,omitempty"`
	Alias       []string  `json:"alias"`
	Regions     []string  `json:"regions"`
	Coordinates []float64 `json:"coordinates,omitempty"`
	Province    string    `json:"province,omitempty"`
	Timezone    string    `json:"timezone,omitempty"`
	Unlocs      []string  `json:"unlocs,omitempty"`
	Code        string    `json:"code,omitempty"`
}
