package data

import (
	"encoding/json"

	"github.com/rs/zerolog/log"
)

func ExtractArrayString(decoder *json.Decoder) ([]string, error) {
	funcName := "ExtractArrayString"
	stringsList := []string{}

	tokensList, err := extractor(decoder)
	if err != nil {
		log.Err(err).
			Str("Function", funcName).
			Msg("can't fetch tokensList")
		return nil, err
	}

	for _, s := range tokensList {
		if value, isString := s.(string); isString {
			stringsList = append(stringsList, value)
		}
	}

	return stringsList, nil
}

func ExtractArrayFloat64(decoder *json.Decoder) ([]float64, error) {
	funcName := "ExtractArrayFloat64"
	float64list := []float64{}

	tokensList, err := extractor(decoder)
	if err != nil {
		log.Err(err).
			Str("Function", funcName).
			Msg("can't fetch tokensList")
		return nil, err
	}

	for _, floatNum := range tokensList {
		if value, isFloat64 := floatNum.(float64); isFloat64 {
			float64list = append(float64list, value)
		}
	}

	return float64list, nil
}

func extractor(decoder *json.Decoder) ([]json.Token, error) {
	tokensList := []json.Token{}

	_, err := decoder.Token()
	if err != nil {
		return nil, err
	}

	for decoder.More() {
		t, err := decoder.Token()
		if err != nil {
			return nil, err
		}
		tokensList = append(tokensList, t)
	}

	_, err = decoder.Token()
	if err != nil {
		return nil, err
	}

	return tokensList, nil
}
