package data

import (
	"encoding/json"

	"github.com/rs/zerolog/log"
)

func ProcessJSONFields(decoder *json.Decoder, portsData PortsData) (PortsData, error) {
	funcName := "ProcessJSONFields"

	code := ""
	for k, _ := range portsData {
		code = k
	}
	portsDetails := portsData[code]

	for decoder.More() {
		t, err := decoder.Token()
		if err != nil {
			log.Err(err).
				Str("Function", funcName).
				Msg("expected opening value token")
			return portsData, err
		}

		if delim, ok := t.(json.Delim); !ok || delim != '{' {
			log.Err(err).
				Str("Function", funcName).
				Msg(`expected opening bracket "{"`)
			continue
		}

		// Port details
		for decoder.More() {

			t, err = decoder.Token()
			if err != nil {
				log.Err(err).
					Str("Function", funcName).
					Msg("expected opening value")
				return portsData, err
			}

			if name, isString := t.(string); isString {

				switch name {
				case "name":
					t, err = decoder.Token()
					if err != nil {
						log.Err(err).
							Str("Function", funcName).
							Msg("can't get name value")
						return portsData, err
					}
					if _, isString := t.(string); !isString {
						log.Err(err).
							Str("Function", funcName).
							Msg("name value is not string")
						return portsData, err
					}
					portsDetails.Name = t.(string)

				case "city":
					t, err = decoder.Token()
					if err != nil {
						log.Err(err).
							Str("Function", funcName).
							Msg("can't get city value")
						return portsData, err
					}
					if _, isString := t.(string); !isString {
						log.Err(err).
							Str("Function", funcName).
							Msg("city value is not string")
						return portsData, err
					}
					portsDetails.City = t.(string)

				case "country":
					t, err = decoder.Token()
					if err != nil {
						log.Err(err).
							Str("Function", funcName).
							Msg("can't get country value")
						return portsData, err
					}
					if _, isString := t.(string); !isString {
						log.Err(err).
							Str("Function", funcName).
							Msg("country value is not string")
						return portsData, err
					}
					portsDetails.Country = t.(string)

				case "alias":
					aliasesList, err := ExtractArrayString(decoder)
					if err != nil {
						log.Err(err).
							Str("Function", funcName).
							Strs("aliasesList", aliasesList).
							Msg("can't extract aliases")
					}
					portsDetails.Alias = aliasesList

				case "regions":
					regionsList, err := ExtractArrayString(decoder)
					if err != nil {
						log.Err(err).
							Str("Function", funcName).
							Strs("regionsList", regionsList).
							Msg("can't extract regions")
					}
					portsDetails.Regions = regionsList

				case "coordinates":
					coordinatesList, err := ExtractArrayFloat64(decoder)
					if err != nil {
						log.Err(err).
							Str("Function", funcName).
							Floats64("regionsList", coordinatesList).
							Msg("can't extract regions")
					}
					portsDetails.Coordinates = coordinatesList

				case "province":
					t, err = decoder.Token()
					if err != nil {
						log.Err(err).
							Str("Function", funcName).
							Msg("can't get province value")
						return portsData, err
					}
					if _, isString := t.(string); !isString {
						log.Err(err).
							Str("Function", funcName).
							Msg("province value is not string")
						return portsData, err
					}
					portsDetails.Province = t.(string)

				case "timezone":
					t, err = decoder.Token()
					if err != nil {
						log.Err(err).
							Str("Function", funcName).
							Msg("can't get timezone value")
						return portsData, err
					}
					if _, isString := t.(string); !isString {
						log.Err(err).
							Str("Function", funcName).
							Msg("timezone value is not string")
						return portsData, err
					}
					portsDetails.Timezone = t.(string)

				case "unlocs":
					unlocsList, err := ExtractArrayString(decoder)
					if err != nil {
						log.Err(err).
							Str("Function", funcName).
							Strs("regionsList", unlocsList).
							Msg("can't extract regions")
					}
					portsDetails.Unlocs = unlocsList

				case "code":
					t, err = decoder.Token()
					if err != nil {
						log.Err(err).
							Str("Function", funcName).
							Msg("can't get code value")
						return portsData, err
					}
					if _, isString := t.(string); !isString {
						log.Err(err).
							Str("Function", funcName).
							Msg("code value is not string")
						return portsData, err
					}
					portsDetails.Code = t.(string)
				}
			}
		}
	}
	portsData[code] = portsDetails

	return portsData, nil
}
