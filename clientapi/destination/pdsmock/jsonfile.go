package pdsmock

import (
	"encoding/json"
	"io/ioutil"
)

func (s *PDSMockService) saveJSON() error {
	f, err := json.MarshalIndent(s.RecordsDB, "", " ")
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(defaultOutputFile, f, 0644)
	if err != nil {
		return err
	}

	return nil
}
