package pdsmock

import (
	"bytes"
	"clientapi/common/data"
	"sync"

	"github.com/rs/zerolog/log"
)

type PDSMockService struct {
	Config      *PDSMockConfig
	RunOnce     sync.Once
	Data        bytes.Buffer
	RecordExist map[string]bool
	RecordsDB   map[string]data.PortsDataDetails
}

func NewPDSMockService(config *PDSMockConfig) *PDSMockService {
	log.Debug().Str("Adapter", config.ServiceName).Msg("Starting....")
	return &PDSMockService{
		config,
		sync.Once{},
		bytes.Buffer{},
		make(map[string]bool),
		make(map[string]data.PortsDataDetails),
	}
}

func (s *PDSMockService) Init(wantMock bool) error {

	// It's mock, nothing special here
	_ = wantMock
	return nil
}
