package pdsmock

import (
	"clientapi/common/data"
	"encoding/json"

	"github.com/rs/zerolog/log"
)

func (s *PDSMockService) SendData(decoder *json.Decoder) error {

	log.Info().
		Str("Adapter", s.Config.ServiceName).
		Msg("starting reading data")

	t, err := decoder.Token()
	if err != nil {
		log.Err(err).
			Str("Adapter", s.Config.ServiceName).
			Msg("expected opening object - aborted (wrong JSON?)")
		return err
	}

	if delim, ok := t.(json.Delim); !ok || delim != '{' {
		log.Err(err).
			Str("Adapter", s.Config.ServiceName).
			Msg("expected object - wrong JSON?")
		return err
	}

	for decoder.More() {
		t, err := decoder.Token()
		if err != nil {
			log.Err(err).
				Str("Adapter", s.Config.ServiceName).
				Msg("expected object in loop - wrong JSON?")
			return err
		}

		pData := data.PortsData{}
		portsDetails := data.PortsDataDetails{}
		pData[t.(string)] = portsDetails

		portsData, err := data.ProcessJSONFields(decoder, pData)
		if err != nil {
			log.Err(err).
				Str("Adapter", s.Config.ServiceName).
				Interface("portsData", portsData).
				Msg("could not process some json fields")
			return err
		}

		t, err = decoder.Token()
		if err != nil {
			log.Err(err).
				Str("Adapter", s.Config.ServiceName).
				Msg("expected closing array")
			return err
		}

		bytes, err := json.Marshal(portsData)
		if err != nil {
			log.Err(err).
				Str("Adapter", s.Config.ServiceName).
				Interface("portsDetails", portsData).
				Msg("can't marshal this data")
			return err
		}

		// save to buffer (one JSON record)
		s.Data.Write(bytes)

		// process this record and take struct representing chunk
		portsDataForDatabase, err := data.ProcessPortDataChunkFromBuffer(&s.Data)
		if err != nil {
			return err
		}

		// save or update mock database
		databaseWasUpdated, err := s.AddToOrUpdateDatabase(portsDataForDatabase)
		if err != nil {
			return err
		}

		// if data Was updated - update file which is database
		if databaseWasUpdated {
			err := s.saveJSON()
			if err != nil {
				log.Err(err).
					Str("Adapter", s.Config.ServiceName).
					Str("File", defaultOutputFile).
					Msg("cannot save json file")
			}
		}
	}

	_, err = decoder.Token()
	if err != nil {
		log.Err(err).
			Str("Adapter", s.Config.ServiceName).
			Msg("expected closing object - aborted (wrong JSON?)")
		return err
	}

	return nil
}
