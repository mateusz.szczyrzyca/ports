package pdsmock

import (
	"clientapi/common/data"

	"github.com/rs/zerolog/log"
)

func (s *PDSMockService) AddToOrUpdateDatabase(data data.PortsData) (bool, error) {
	funcName := "AddToOrUpdateDatabase"

	databaseWasUpdated := false

	for key, portsDetails := range data {

		if portsDetails.Name == "" || portsDetails.City == "" {
			log.Warn().
				Str("Adapter", s.Config.ServiceName).
				Str("Function", funcName).
				Str("KEY", key).
				Msg("invalid record - not processed")
			continue
		}

		// Adding a new record
		if !s.RecordExist[key] {
			log.Debug().
				Str("Adapter", s.Config.ServiceName).
				Str("Function", funcName).
				Str("KEY", key).
				Msg("Key does not exist - creating a new record")
			s.RecordExist[key] = true
			s.RecordsDB[key] = portsDetails
			databaseWasUpdated = true
			continue
		}

		// Just updating existing record
		log.Debug().
			Str("Adapter", s.Config.ServiceName).
			Str("Function", funcName).
			Str("KEY", key).
			Msg("Record already exist - updating record")
		databaseWasUpdated = true
	}

	return databaseWasUpdated, nil
}
