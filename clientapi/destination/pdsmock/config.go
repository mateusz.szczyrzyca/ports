package pdsmock

type PDSMockConfig struct {
	ServiceName  string
}

func NewPDSMockConfig() *PDSMockConfig {
	return &PDSMockConfig{
		defaultServiceName,
	}
}