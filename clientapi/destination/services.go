package destination

import (
	"clientapi/destination/http"
	"clientapi/destination/pdsmock"
)

type DestinationServices []Destination

func GetDestinationServices() DestinationServices {

	servicesList := []Destination{
		// Destination Service: HTTP Endpoint
		//&http.HTTPDestinationService{
		//	Config: http.NewHTTPDestinationConfig("", "", 0),
		//},
		http.NewHTTPDestinationService(
			http.NewHTTPDestinationConfig("", "", 0),
		),
		pdsmock.NewPDSMockService(
			pdsmock.NewPDSMockConfig(),
			),
	}

	return servicesList
}
