package http

import (
	"io"
	"net/http"

	"github.com/rs/zerolog/log"
)

func (s *HTTPDestinationService) HTTPProvideData(w http.ResponseWriter, r *http.Request) {
	log.Info().
		Str("Service", s.Config.ServiceName).
		Str("IP", r.RemoteAddr).
		Str("URI", r.RequestURI).
		Msg("Request arrived.")
	io.WriteString(w, "Welcome!")
}
