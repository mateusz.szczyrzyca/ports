package http

const (
	defaultServiceName = "HTTPDestinationService"
	defaultEndpointName = "/portdata"
	defaultEndpointPort = 8080
)
