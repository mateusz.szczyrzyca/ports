package http

import (
	"fmt"
	"net/http"
	"sync"

	"github.com/rs/zerolog/log"
)

type HTTPDestinationService struct {
	Config  *HTTPDestinationConfig
	RunOnce sync.Once
}

func NewHTTPDestinationService(config *HTTPDestinationConfig) *HTTPDestinationService {
	return &HTTPDestinationService{
		config,
		sync.Once{},
	}
}

func (s *HTTPDestinationService) Init(wantMock bool) error {

	if wantMock {
		log.Warn().Str("Adapter", s.Config.ServiceName).Msg("Mock requested - aborting")
		return nil
	}

	// Run only once
	s.RunOnce.Do(
		func() {
			log.Info().Str("Adapter", s.Config.ServiceName).Msg("Starting...")
			portDataEndpoint := http.NewServeMux()
			portDataEndpoint.HandleFunc(s.Config.EndpointName, s.HTTPProvideData)

			go func() {
				http.ListenAndServe(fmt.Sprintf(":%d", s.Config.EndpointPort), portDataEndpoint)
			}()

		})

	return nil
}
