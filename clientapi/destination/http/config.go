package http

type HTTPDestinationConfig struct {
	ServiceName  string
	EndpointName string
	EndpointPort int
}

func NewHTTPDestinationConfig(serviceNameStr, endpointNameStr string, port int) *HTTPDestinationConfig {

	serviceName := setServiceName(serviceNameStr)
	endpointName := setEndpointName(endpointNameStr)
	endpointPort := setEndpointPort(port)


	return &HTTPDestinationConfig{
		serviceName,
		endpointName,
		endpointPort,
	}
}

func setServiceName(name string) string {
	if name == "" {
		return defaultServiceName
	}

	return name
}


func setEndpointName(name string) string {
	if name == "" {
		return defaultEndpointName
	}

	return name
}

func setEndpointPort(port int) int {
	if port <= 0 {
		return defaultEndpointPort
	}

	return port
}
