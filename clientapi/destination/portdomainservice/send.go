package portdomainservice

import (
	"clientapi/common/data"
	"encoding/json"
	"github.com/rs/zerolog/log"
	"io"
)

func (s *PDSService) SendData(decoder *json.Decoder) error {

	var portsData data.PortsData
	for {
		if err := decoder.Decode(&portsData); err == io.EOF {
			break
		} else if err != nil {
			log.Err(err).Msg("problem with dataSourceDecoder")
		}
	}

	for key, value := range portsData {
		// Adding new record
		if !s.RecordExist[key] {
			log.Debug().
				Str("Adapter", s.Config.ServiceName).
				Str("KEY", key).
				Msg("Key does not exist - creating a new record")
			s.RecordExist[key] = true
			s.Data[key] = value
			continue
		}
		// Just updating record
		log.Debug().
			Str("Adapter", s.Config.ServiceName).
			Str("KEY", key).
			Msg("Record already exist - updating record")
		s.Data[key] = value
	}

	return nil
}

