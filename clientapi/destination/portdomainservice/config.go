package portdomainservice

type PDSConfig struct {
	ServiceName  string
}

func NewPDSConfig() *PDSConfig {
	return &PDSConfig{
		defaultServiceName,
	}
}