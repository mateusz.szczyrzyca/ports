package portdomainservice

import (
	"clientapi/common/data"
	"github.com/rs/zerolog/log"
	"sync"
)

type PDSService struct {
	Config  *PDSConfig
	RunOnce sync.Once
	Data        data.PortsData
	RecordExist map[string]bool
}

func NewPDSMockService(config *PDSConfig) *PDSService {
	log.Debug().Str("Adapter", config.ServiceName).Msg("Starting....")
	return &PDSService{
		config,
		sync.Once{},
		make(map[string]data.PortsDataDetails),
		make(map[string]bool),
	}
}

func (s *PDSService) Init(wantMock bool) error {

	if wantMock {
		log.Debug().Str("Adapter", s.Config.ServiceName).Msg("mock requested - aborting operation")
		return nil
	}
	return nil
}
