package destination

import "encoding/json"

// Destination needs to be implemented by subpackages
type Destination interface {
	Init(bool) error
	SendData(*json.Decoder) error
}