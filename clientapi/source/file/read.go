package file

import (
	"encoding/json"
	"github.com/rs/zerolog/log"
	"os"
)

func (s *FileSourceService) ReadPortsData() (*json.Decoder, error) {

	fileName := defaultPortsFile
	file, err := os.Open(fileName)
	if err != nil {
		log.Err(err).
			Str("Adapter", s.Config.ServiceName).
			Str("File", fileName).
			Msg("can't read the ports file")
		return &json.Decoder{}, err
	}

	return json.NewDecoder(file), nil
}
