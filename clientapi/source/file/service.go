package file

type FileSourceService struct {
	Config *FileSourceConfig
	TestData []byte
}

func NewFileSourceService(config *FileSourceConfig) *FileSourceService {
	return &FileSourceService{
		config,
		nil,
	}
}

