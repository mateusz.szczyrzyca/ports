package file

type FileSourceConfig struct {
	ServiceName string
}

func NewFileSourceConfig() *FileSourceConfig {
	return &FileSourceConfig{
		defaultServiceName,
	}
}

