package source

import (
	"encoding/json"
)

/*
DataSource
	This interface needs to be implemented by subpackages
	Provides data source for domain

 */
type DataSource interface {
	ReadPortsData() (*json.Decoder, error)
}