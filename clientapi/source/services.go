package source

import "clientapi/source/file"

type SourceServices []DataSource

func GetSourceServices() SourceServices {

	servicesList := []DataSource{
		file.NewFileSourceService(
			file.NewFileSourceConfig(),
		),
	}

	return servicesList
}
