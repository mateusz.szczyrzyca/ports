## What's here

1) `clientapi` - microservice which is responsible for reading `ports.json` file and exposing REST API interface for access data from `portdomainservice`. The microservice sends gRPC request to `portdomainservice` per one record from the file as it was stated in requirements

2) `portdomainservice`
   Microservice which exposes gRPC protocol to receive data from `clientapi` and updates or saves (if it does not exist) records in it's in memory map database. This microservice also sends all data to `clientapi` if it's requested in different route. 
   The microservice uses simple map as it's database, this map is saved on disc upon each insert/update of record. To preserve data, binary `encoding/gob` from standard library is used as a serialization protocol for simplicity.

Both microservices are written in a DDD-similar architecture and can be deployed by `docker-compose` command. To get more details about this architecture see `Documentation/Architecture.md` file which explains in detail architectural concept used here.

To know what's implemented and how, according to the requirements, please look at `Documentation/Summary.md` file where it's explained in detail.

## Install/Start an app

### Normal start
To start the system just type `make start` In order to start microservices the `docker-compose.yml` file will be used, so it will build two microservices within docker containers.

`Makefile` it's provided for simplicity, but it's possible to run using only `docker-compose` command by:

```bash
docker-compose build --no-cache && docker-compose up
```

### Mock test (test in isolation)

This is special test-diagnostic mode which starts microservices wit isolation - each microservice in this mode won't communicate with 
external world and use it's on mocks. So, for instance, `clientapi` microservice will use `portdomainservice` mock which is called `pdsmock`.

### System test

This is test which happens on "normal" working system