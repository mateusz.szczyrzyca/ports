## Begin

Architecture described here is neither best and easiest, but this is what I think should be if we are talking about flexibility, DDD concepts, tests, etc.

**For these two microservices this architecture is mostly overkill, but I decided to use it to show my thinking about how scalable systems should be developed**

For picture, please go to `Documentation/ArchitecturePicture.pdf` which shows layers and architecture depicted.

## Domain

Domain is a "central" part of the code - here's the main logic happens. Each change of domain code is critical for application 
as it changes logic. **Domain code should not be changed unless** it's really needed and justified. In these microservices domain is presented at `domain.go` file which is at top level directory.

Domains only "sees" the nearest layers - in this example the closest layer is `Infrastructure` layer. 
Domain should not "know" anything beyond `Infrastructure`

There is also `common` directory which shares common concepts for all layers. This is part of the domain, so changes at this directory can brake down the system.

## Infrastructure

Because the domain operates purely on interfaces methods, there should be place which provides such interface. And this is 
exactly `Infrastructure` layer which "separates" `domain` and `adapters` layers. This is kind of "bridge".

This later provides interface methods for domain which are used there. Moreover, additional interfaces are defined in this 
layer, and these interfaces will be automatically implement by `Infrastructure` adapters.

`Infrastructure` knows about adapters existence - adapters needs to be added to services list. Hence, each adding/removing 
adapter needs small change in `services.go` in `Infrastructure` layer.
This brings us to another trait: because `Infrastructure` (with the same type) "knows" which adapters are attached to it, 
it can provide logic to route data between it's own adapters. If there is requirement that data needs to be shared between 
two or more adapters - that action needs take place in `Infrastructure` layer

## Adapters

`Adapter` is a subpackage which provides functionality for `Infrastructure` layer. Adapter SHOULD NOT be aware that 
any another adapter exists. Each adapter should "think" it's the only one in the Universe and only it implements 
`Infrastucture` interface. This implementation is done automatically due to Go compiler, hence adapters really 
"don't know" if they implement something or not.

Hence adapters are very flexibe in this architecture: for example, in `source` (`Infrastructure` layer) there can be 
adapter which read data from file, any kind of database or message bus. 

We can have the following adapters (these are just examples):

- `file` - adapter which takes data from file (current scenario)
- `mysql` - this adapter may take data from mysql database, mysql-depended libraries will exist only inside this adapter
            and won't "poison" another packages
- `pgsql` - this adapter may take from pgsql db. Same scenario as above - libs needed to use PostgreSQL will exist only
            inside this adapter
- `kafka` - adapter which connects to kafka cluster and takes data from it. Any kafka related dependencies will exist
            only inside this adapter

In such example, these adapters `file`, `mysql`, `pgsql` and `kafka` completely don't know about their existence. Their 
code base is totally separated, only one "common" thing is that they should implement interfaces from `Infrastructure` layer, 
but "they don't know it" as it's done automatically in Go.


## Adding new adapter for source to Infrastructure layer

To add a new adapter, we need to: 

1) We have to create directory under `source/` - for instance: `mysql` for adapter to provide data from MySQL data. If this adapter is supposed to work as a destination for data, then `destination/` infra layer is much better place.

2) Add calling MySQL adapter constructor in `services.go` files (`source/services.go` or `destination/services.go`)

That's it - rest of the code should not be changed, that's the priority for this architecture.

## Advantages and disadvantages

Advantages of this architecture:

1) Domain logic should be untouched as long as possible, it's critical part of the application, should only use interfaces provided by `Infrastructure` layers

2) In this example there two "types" of `Infrastructure` layer:
   - source - this layer provides access to data from various sources (adapters)
   - destination - this layer gives access to adapters which expects data obtained by `source` adapters 
                   and process this data or forward it further

   Domains "knows" it's the nearest layer is `Infrastructure` and in this example there are two 
   Infra-layers. Hence domain can use interface methods from both packages.

3) Adapters not share any code with any part of infra, they don't know about existence of each other, they 
   live only inside their own worlds. It's strictly prohibited that any adapter could use something more
   than core `common` package.
   
   Changing adapter code does not affect:

   - another adapters
   - domain logic

4) Because layers only use interfaces from nearest layers, mocking or testing is much easier as we can just test behavior. 


## Cons

1) Architecture is complicated and not suitable for very simple projects like single responsibility microservices. There are no significant benefits here while using such architecture.
