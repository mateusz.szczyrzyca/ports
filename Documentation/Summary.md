## What has been done

- Detailed logging to stdout with couple log levels
- Good and scalable architecture, but very complicated for this cask
- Dockerfile, Docker compose and Makefile
- Small documentation

## What's missing or what's simplified and why

Because of the statement from README - don't spend more than 2 hours - this is impossible to fulfil 
such requirements in 2h. This type of architecture was designing for around 2 weeks and it's used in my side projects.

In both microservices:

1) Most data are taken from constats at `defaults.go` to do not produce more code, 
   but in a normal system, it can be fetched from command line arguments or environment 
   variables (this is recommended for containers)

   How such data is handled is presented at `clientapi/destination/http` adapter, hence:

   - we can pass values in a config constructor (`NewHTTPDestinationConfig`) - we may
     produce many such config objects but with different configurations. Using them we can
     create many service objects with different configs.

   - if no values are passed, then default values from `defaults.go` will be used (in most adapters 
     this is the case)

2) Tests

[CLIENT API]

1) mock for file - file can be treated itself as a mock.
2) gRPC client to portdomainservice 
3) Rest API endpoint is listening in non mock mode but it's not finished (no logic written)

[PORTDOMAINSERVICE]

1) gob database - data retrieval not supported (only saving as *.gob file), this is only for presenting simple database concept for docker container.