package mock

import "github.com/rs/zerolog/log"

func (s *MockSourceService) Init(wantMock bool) error {
	if !wantMock {
		log.Warn().
			Str("Adapter", s.SvcName).Msg("I'm a mock, not real server - aborting")
		return nil
	}

	s.Running = true

	return nil
}
