package mock

const (
	defaultServiceName   = "MockPortsFile"
	defaultPortsDataFile = "./source/mock/data/ports.json"
)
