package mock

type MockSourceService struct {
	SvcName       string
	Config        *MockSourceConfig
	Running       bool
	DataIsFetched bool
	Data          []byte
}

func NewMockSourceService(config *MockSourceConfig) *MockSourceService {
	return &MockSourceService{
		defaultServiceName,
		config,
		false,
		false,
		nil,
	}
}
