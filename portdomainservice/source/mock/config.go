package mock

type MockSourceConfig struct {
	PortDataFile string
}

func NewMockSourceConfig() *MockSourceConfig {
	return &MockSourceConfig{
		PortDataFile: defaultPortsDataFile,
	}
}
