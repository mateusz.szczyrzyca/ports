package mock

import (
	"io/ioutil"

	"github.com/rs/zerolog/log"
)

func (s *MockSourceService) ReadPortsData() ([]byte, error) {

	// prevent re-read same file again
	if s.DataIsFetched {
		log.Debug().Str("Adapter", s.SvcName).
			Msg("data fetched - returning from cache")
		return s.Data, nil
	}

	if !s.Running {
		log.Warn().Str("Adapter", s.SvcName).
			Bool("IsRunning", s.Running).
			Msg("service is not running")
		return nil, nil
	}

	bytes, err := ioutil.ReadFile(s.Config.PortDataFile)
	if err != nil {
		log.Err(err).Str("Adapter", s.SvcName).
			Str("File", s.Config.PortDataFile).
			Msg("problem with reading file")
		return nil, err
	}

	s.DataIsFetched = true
	s.Data = bytes

	log.Info().Str("Adapter", s.SvcName).
		Int("Bytes size", len(bytes)).
		Msg("Data fetched from the file")
	return bytes, nil
}
