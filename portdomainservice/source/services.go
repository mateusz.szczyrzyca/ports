package source

import (
	"portdomainservice/source/grpc"
	"portdomainservice/source/mock"
)

type SourceServices []DataSource

func GetSourceServices() SourceServices {

	servicesList := []DataSource{
		mock.NewMockSourceService(
			mock.NewMockSourceConfig(),
		),
		grpc.NewGRPCService(
			grpc.NewGRPCConfig(),
		),
	}

	return servicesList
}
