package grpc

import (
	"net"

	"github.com/rs/zerolog/log"
	grpc2 "google.golang.org/grpc"
)

func (s *GRPCService) Init(wantMock bool) error {

	if wantMock {
		log.Warn().
			Str("Adapter", s.Config.serviceName).
			Msg("mocks requested - I'm a real server, aborting")
		return nil
	}

	var possibleError error = nil

	s.RunOnce.Do(func() {
		lis, err := net.Listen("tcp", defaultAddress)
		if err != nil {
			log.Err(err).
				Str("Adapter", s.Config.serviceName).
				Msg("cannot listen for grpc server")
			possibleError = err
		}

		srv := grpc2.NewServer()
		RegisterPortsDataServer(srv, &gRPCserver{})
		if err := srv.Serve(lis); err != nil {
			log.Err(err).
				Str("Adapter", s.Config.serviceName).
				Msg("cannot start grpc server")
			possibleError = err
		}

		if possibleError == nil {
			s.Running = true
		}
	})

	return possibleError
}
