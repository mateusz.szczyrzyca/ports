package grpc

import (
	context "context"

	"github.com/rs/zerolog/log"
)

type gRPCserver struct {
	UnimplementedPortsDataServer
}

func (s *gRPCserver) PortsData(ctx context.Context, in *PortsRequest) (*PortsReply, error) {
	log.Info().
		Str("Adapter", "grpc").
		Interface("Received: %v", in.Message).Msg("grpc request")

	return &PortsReply{Message: "OK"}, nil
}

func (s *GRPCService) ReadPortsData() ([]byte, error) {

	if !s.Running {
		log.Warn().Str("Adapter", s.Config.serviceName).
			Bool("IsRunning", s.Running).
			Msg("service is not running")
		return nil, nil
	}

	return nil, nil
}
