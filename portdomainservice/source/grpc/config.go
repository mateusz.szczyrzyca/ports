package grpc

type GRPCConfig struct {
	serviceName string
}

func NewGRPCConfig() *GRPCConfig {
	return &GRPCConfig{
		defaultServiceName,
	}
}
