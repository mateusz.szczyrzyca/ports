package grpc

import "sync"

type GRPCService struct {
	Config  *GRPCConfig
	Running bool
	RunOnce *sync.Once
}

func NewGRPCService(config *GRPCConfig) *GRPCService {
	return &GRPCService{
		config,
		false,
		&sync.Once{},
	}
}
