package source

/*
DataSource
	This interface needs to be implemented by subpackages
	Provides data source for domain
*/
type DataSource interface {
	Init(bool) error
	ReadPortsData() ([]byte, error)
	GetName() string
}
