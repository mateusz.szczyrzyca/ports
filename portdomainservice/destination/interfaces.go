package destination

// Destination needs to be implemented by subpackages
type Destination interface {
	SaveData([]byte) error
	GetName() string
}
