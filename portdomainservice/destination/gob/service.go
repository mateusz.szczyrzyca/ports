package gob

type GobDestService struct {
	SvcName   string
	Config    *GobDestConfig
	DBVersion int64
}

func NewGobDestService(config *GobDestConfig) *GobDestService {
	return &GobDestService{
		defaultServiceName,
		config,
		0,
	}
}

func (s *GobDestService) GetName() string {
	return s.SvcName
}
