package gob

type GobDestConfig struct {
	DataFile string
}

func NewGobDestConfig() *GobDestConfig {
	return &GobDestConfig{
		DataFile: defaultDataFile,
	}
}
