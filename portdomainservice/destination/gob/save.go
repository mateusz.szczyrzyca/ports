package gob

import (
	"bytes"
	"encoding/gob"
	"io/ioutil"

	"github.com/rs/zerolog/log"
)

func (s *GobDestService) SaveData(data []byte) error {

	dataBytes, err := s.encode(data)
	if err != nil {
		log.Err(err).
			Str("Adapter", s.SvcName).Msg("cannot encode data")
		return err
	}

	err = s.saveGobFile(dataBytes)
	if err != nil {
		log.Err(err).
			Str("Adapter", s.SvcName).
			Str("file", s.Config.DataFile).
			Msg("cannot save data to file")
		return err
	}

	s.DBVersion++

	return nil
}

func (s *GobDestService) encode(portsData []byte) ([]byte, error) {
	var buf bytes.Buffer
	encoder := gob.NewEncoder(&buf)
	err := encoder.Encode(portsData)
	if err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

func (s *GobDestService) saveGobFile(portsData []byte) error {
	err := ioutil.WriteFile(s.Config.DataFile, portsData, 0644)
	if err != nil {
		return err
	}
	return nil
}
