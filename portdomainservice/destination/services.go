package destination

import "portdomainservice/destination/gob"

type DestinationServices []Destination

func GetDestinationServices() DestinationServices {

	servicesList := []Destination{
		// Destination Service: HTTP Endpoint
		gob.NewGobDestService(
			gob.NewGobDestConfig(),
		),
	}

	return servicesList
}
