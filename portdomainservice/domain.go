package main

import (
	"os"
	"portdomainservice/destination"
	"portdomainservice/source"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

func mainLoop(areMocksEnabled bool) {

	sourceServicesList := source.GetSourceServices()
	destinationServices := destination.GetDestinationServices()

	log.Info().Strs("ENV VARIABLES", os.Environ()).Send()

	log.Info().Msg("Starting application")
	for {
		portsD := make([]byte, 0)

		log.Debug().Msg("Source loop <-")
		for _, sourceService := range sourceServicesList {

			err := sourceService.Init(areMocksEnabled)
			if err != nil {
				log.Fatal().Err(err).
					Str("Service", sourceService.GetName()).
					Msg("critical error: can't initialize source service.")
			}

			ports, err := sourceService.ReadPortsData()
			if err != nil {
				log.Fatal().Err(err).
					Str("Service", sourceService.GetName()).
					Msg("critical error: can't read data from one data source service.")
			}
			portsD = ports
			// TODO:
			// if mock:
			//   map []byte to map here once and keep it
			//
			// if data comes from grpc (port by port)
			//   1. check if record ID exist in memory
			//      - update if yes
			//      - add if no
			//
			// later map usage in destination.grpc (asked by clientapi by request to clientapi)
			// later map usage in destination.gob (saving database from memory to gob, remember db version)
		}

		log.Debug().Msg("Destination loop ->")
		for _, destService := range destinationServices {
			err := destService.SaveData(portsD)
			if err != nil {
				log.Fatal().Err(err).
					Str("Service", destService.GetName()).
					Msg("critical error: can't read data from one data source service.")
			}
		}

		time.Sleep(1 * time.Second)
	}
	log.Info().Msg("Application stopped.")
}

func main() {

	// env variables

	// turning on mocks allows test as application "lives" in isolation
	logLevel := "Info"
	areMocksEnabled := false
	if os.Getenv("APP_PDS_MOCK_ENABLE") == "true" {
		areMocksEnabled = true
	}

	if os.Getenv("APP_PDS_DEBUG") == "true" {
		logLevel = "DEBUG"
	}

	// Default level for this example is info, unless debug flag is present
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if logLevel == "DEBUG" {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	mainLoop(areMocksEnabled)

}
