export PATH := $(PWD)/go/bin:$(PATH)

install:

start:
	docker-compose build --no-cache && docker-compose up

stop:
	docker-compose down

mock:
	go get -v github.com/josephburnett/jd ; \
	export APP_PDS_MOCK_ENABLE=true ; \
	export APP_PDS_DEBUG=true ; \
	export APP_CLIENTAPI_MOCK_ENABLE=true ; \
	export APP_CLIENTAPI_DEBUG=true ; \
	docker-compose build --no-cache && docker-compose up -d  

jsontest:
	go get -v github.com/josephburnett/jd ; \
	JDCMP := $(shell jd -set pdsmock/pdsmock.json clientapi/source/file/data/ports.json)
	ifneq ($(JDCMP),0)
	    $(error "json files are different - something is wrong")
	endif

clean:
	rm -f clientapi/podsmock/pdsmock.json ; \
	rm -f podsmock/pdsmock.json ; \
	rm -rf database/*.gob
